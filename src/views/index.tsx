// @ts-nocheck
import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import AppLayout from "../layouts/app-layout";
import AuthLayout from '../layouts/auth-layout';
import AppLocale from "../lang";
import { IntlProvider } from "react-intl";
import { ConfigProvider } from 'antd';
import { AUTH_PREFIX_PATH } from '../configs/AppConfig'
import useBodyClass from '../hooks/useBodyClass';


export const Views = (props: any) => {
  const { locale, location, direction } = props;
  const currentAppLocale = AppLocale[locale]; 
  useBodyClass(`dir-${direction}`);
  return (
    <IntlProvider
      locale={currentAppLocale.locale}
      messages={currentAppLocale.messages}>
      <ConfigProvider locale={currentAppLocale.antd} direction={direction}>
        <Switch>
          <Route path={AUTH_PREFIX_PATH}>
            <AuthLayout direction={direction} />
          </Route>
          <Route path={'/'}>
            <AppLayout direction={direction} location={location}/>
          </Route>
        </Switch>
      </ConfigProvider>
    </IntlProvider>
  )
}

const mapStateToProps = ({ theme, auth }) => {
  const { locale, direction } =  theme;
  return { locale, direction }
};

export default withRouter(connect(mapStateToProps)(Views));