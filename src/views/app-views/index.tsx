import React, { lazy, Suspense } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Loading from "../../components/shared-components/Loading";

const VpsComponent = lazy(() => import("./vps"));

const AppViews = () => {
  return (
    <Suspense fallback={<Loading cover="content" />}>
      <Switch>
        <Route path="/vps" component={VpsComponent} />
        <Redirect from={`/`} to={`/home`} />
      </Switch>
    </Suspense>
  );
};

export default React.memo(AppViews);
