import { PlusOutlined } from "@ant-design/icons";
import { Button, Card } from "antd";
import React, { useCallback, useState } from "react";
import VpsTable from "../../../components/app-components/vps-components/VpsTable";
import VpsModal from "../../../components/modals/VpsModal";
import { FormMode, Vps } from "../../../types";

const VpsComponent = () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [mode, setMode] = useState<FormMode>("ADD");
  const [vpsEdit, setVpsEdit] = useState<Vps | null>(null);

  const toogleVisible = useCallback((value: boolean) => {
    setVisible(value);
    setVpsEdit(null);
  }, []);

  const setModeEdit = useCallback((data: any) => {
    setVpsEdit(data);
    setMode("EDIT");
    setVisible(true);
  }, []);

  return (
    <div>
      <Card
        title="Danh sách Vps"
        extra={[
          <Button
            key="button-1"
            onClick={() => {
              setVisible(true);
              setMode("ADD");
            }}
            icon={<PlusOutlined />}
            size="small"
            type="primary"
          >
            Thêm Vps
          </Button>,
        ]}
      >
        <VpsTable
          data={[]}
          setModeEdit={setModeEdit}
          toogleVisible={toogleVisible}
        />
      </Card>
      <VpsModal
        vpsEdit={vpsEdit}
        visible={visible}
        toogleVisible={toogleVisible}
        mode={mode}
      />
    </div>
  );
};

export default VpsComponent;
