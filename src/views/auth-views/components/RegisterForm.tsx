import React, { useState } from "react";
import { LockOutlined, MailOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Form, Input } from "antd";
import { showNotification } from "../../../utils/common";

export const RegisterForm = () => {
  const [loading, setLoading] = useState(false);
  const [form] = Form.useForm();

  const onSignUp = async (values: {
    username: string;
    email: string;
    password: string;
    fullName: string;
  }) => {
    // const { fullName, username, password, email } = values;
    setLoading(true);
    try {
      showNotification('success', 'Tạo tài khoản thành công');
    } catch (err) {
      console.error('<=!=> tạo tài khoàn thất bại');
    }
    setLoading(false);
  };

  return (
    <>
      <Form
        form={form}
        layout="vertical"
        name="register-form"
        onFinish={onSignUp}
      >
        <Form.Item
          name="fullName"
          label="Họ tên"
          rules={[
            { required: true, message: "Vui lòng nhận tên đầy đủ!" },
            { min: 5, message: "Tên đầy đủ phải có tối thiểu 5 kí tự!" },
          ]}
          hasFeedback
        >
          <Input prefix={<UserOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="username"
          label="Tên tài khoản"
          rules={[
            { required: true, message: "Vui lòng nhận tên tài khoản!" },
            { min: 5, message: "Tên tài khoản phải có tối thiểu 5 kí tự!" },
          ]}
          hasFeedback
        >
          <Input prefix={<UserOutlined className="text-primary"/>}/>
        </Form.Item>
        <Form.Item
          name="email"
          label="Email"
          rules={[
            { required: true, message: "Vui lòng nhập email!" },
            { type: "email", message: "Vui lòng nhập email hợp lệ!" },
          ]}
          hasFeedback
        >
          <Input prefix={<MailOutlined className="text-primary" />} />
        </Form.Item>
        <Form.Item
          name="password"
          label="Password"
          rules={[
            { required: true, message: "Vui lòng nhập mật khẩu!" },
            { min: 8, message: "Mật khẩu phải có tối thiểu 8 kí tự!" },
          ]}
          hasFeedback
        >
          <Input.Password prefix={<LockOutlined className="text-primary" />} />
        </Form.Item>
        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Vui lòng nhập lại mật khẩu!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("Nhập lại mật khẩu không chính xác!")
                );
              },
            }),
          ]}
        >
          <Input.Password prefix={<LockOutlined className="text-primary" />}/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Sign Up
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default RegisterForm;
