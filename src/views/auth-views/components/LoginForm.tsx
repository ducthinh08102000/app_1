import React, { useEffect, useState } from "react";
import { Button, Form } from "antd";

export const LoginForm = () => {
  const [loading, setLoading] = useState(false);

  const onLogin = async (values: {
    username: string;
    password: string;
    remember: boolean;
  }) => {
    try {
    } catch (err) {
      console.error("<=!=> Đăng nhập thất bại");
      console.error(err);
    }
    setLoading(false);
  };

  useEffect(() => {});

  return (
    <>
      <Form layout="vertical" name="login-form" onFinish={onLogin}>
        <Form.Item>
          <Button type="primary" htmlType="submit" block loading={loading}>
            Sign In with NorthStudio Account
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default LoginForm;
