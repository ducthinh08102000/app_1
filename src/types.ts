// Common
export type FormMode = "ADD" | "EDIT";

// Authentication

export interface TokenType {
  access: { token: string; expires: number } | null;
  refresh: { token: string; expires: number } | null;
}

// VPS

export interface Vps {
  id: string;
  name: string;
  numberOfCore: number;
  ram: number;
  hardWare: number;
  status: boolean;
}
