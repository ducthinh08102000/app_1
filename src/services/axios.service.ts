import { notification } from "antd";
import axios from "axios";
import { API_BASE_URL, APP_TOKEN_KEY, AUTH_PREFIX_PATH } from "../configs/AppConfig";
import { TokenType } from "../types";
import { showNotification } from "../utils/common";
import { getData, removeData, setData } from "./storageService";

const handleRefreshToken = async (refreshToken: string): Promise<TokenType> => {
  console.log("==> Refresh token...");
  return new Promise(async (resolve) => {
    try {
      const res = await axios.post(API_BASE_URL + "/auth/token", {
        refresh_token: refreshToken,
        grant_type: "refresh_token",
      });
      console.log("==> Refresh token success");
      resolve(res.data);
    } catch (err) {
      console.log("==> Refresh token failed " + err);
      resolve({ access: null, refresh: null });
    }
  });
};

const axiosService = axios.create({
  timeout: 15000,
  baseURL: API_BASE_URL,
});

axiosService.interceptors.request.use(
  async (config) => {
    // check token
    let _config = config;
    const app_token = await getData<TokenType>(APP_TOKEN_KEY, {
      access: null,
      refresh: null,
    });
    const { access, refresh } = app_token;
    const currentTime = new Date().getTime();
    if (access?.token && access.expires > currentTime) {
      console.log("=> access token: " + access.token);
      return {
        ..._config,
        headers: {
          ..._config.headers,
          Authorization: `Bearer ${access.token}`,
        },
      };
    }
    if (refresh?.token) {
      // hanle refresh token
      try {
        const token = await handleRefreshToken(refresh.token);
        if (token) {
          await setData(APP_TOKEN_KEY, {
            access: token.access,
            refresh: token.refresh,
          });
          _config = {
            ..._config,
            headers: {
              ..._config.headers,
              Authorization: `Bearer ${token.access?.token ?? ''}`,
            },
          };
        }
      } catch (err) {
        console.log(err)
        return _config;
      }
    }
    console.log(_config);
    return _config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

axiosService.interceptors.response.use(
  async (res) => res,
  (err) => {
    if (err["toJSON"]().message === "Network Error") {
      console.log("test", err["toJSON"]());
      notification.error({ message: "Network Error" });
      window.location.replace("/auth/error-2");
    }
    const { status, data } = err.response;
    if(status === 401) {
      removeData('all');
      window.location.replace(AUTH_PREFIX_PATH);
    }

    showNotification("error", data?.error ?? "");
    return Promise.reject(err);
  }
);

export default axiosService;
