export const getData = async <T>(key: string, defaultValue: T): Promise<T> => {
  return new Promise(async (resolve) => {
    try {
      const data = await localStorage.getItem(key);
      if (data) resolve(JSON.parse(data));
      else resolve(defaultValue);
    } catch (err) {
      resolve(defaultValue);
    }
  });
};

export const setData = async (key: string, value: any): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      await localStorage.setItem(key, JSON.stringify(value));
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
};

export const removeData = async (key: string): Promise<any> => {
  return new Promise(async (resolve, reject) => {
    try {
      if (key === "all") {
        localStorage.clear();
        resolve(true);
      }
      await localStorage.removeItem(key);
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
};
