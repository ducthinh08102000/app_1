import { AxiosError, AxiosResponse, Method } from "axios";
import axiosService from "../axios.service";

const dataQuery = (data: { [key: string]: string | number }) => {
  if (!data && Object.keys(data).length! > 0) return "";
  return Object.keys(data).reduce((result, x) => {
    return (result += `${x}=${data[x]}&`);
  }, "?");
};

const request = <T>(
  endpoint: string,
  method: Method,
  data: any
): Promise<T> => {
  return new Promise((resolve, reject) => {
    const url =
      method.toUpperCase() === "GET" ? endpoint + dataQuery(data) : endpoint;
    const _data = method.toUpperCase() === "GET" ? {} : data;
    axiosService
      .request({
        method,
        url,
        data: _data,
      })
      .then((res: AxiosResponse) => resolve(res.data))
      .catch((err: AxiosError) => reject(err));
  });
};

class ApiService {
  static test() {
    return request("/", "get", {});
  }
}

export default ApiService;
