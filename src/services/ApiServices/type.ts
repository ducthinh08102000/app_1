

interface Pagination {
  total: number;
  page: number;
  hasNext: boolean;
  hasPrev: boolean;
}
export interface GetDataRes<T> {
  data: T;
  pagination: Pagination;
}

export interface Param {
  limit?: number;
  page?: number;
  sortBy?: "createdAt" | "-createdAt";
}

