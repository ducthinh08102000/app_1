import { createSlice, PayloadAction } from "@reduxjs/toolkit";


const initialState: any = {
  _id: '',
  fullName: "",
};

const accountSlicce = createSlice({
  name: "account",
  initialState,
  reducers: {
    updateAccount: (state, action: PayloadAction<any>) => {
      return action.payload;
    },
  },
});

export const { updateAccount } = accountSlicce.actions;

export default accountSlicce.reducer;
