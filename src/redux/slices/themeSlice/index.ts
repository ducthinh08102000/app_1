import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { THEME_CONFIG } from "../../../configs/AppConfig";

interface ThemeState {
  navCollapsed: boolean;
  sideNavTheme: string;
  locale: string;
  navType: string;
  topNavColor: string;
  headerNavColor: string;
  mobileNav: boolean;
  currentTheme: string;
  direction: string;
}

const initialState: ThemeState = THEME_CONFIG;

export const themeSlice = createSlice({
  name: "theme",
  initialState,
  reducers: {
    toogleCollapsedNav: (state, action: PayloadAction<boolean>) => {
      state.navCollapsed = action.payload;
    },
    changeSideNavStyle: (state, action: PayloadAction<string>) => {
      state.sideNavTheme = action.payload;
    },
    changeLocal: (state, action: PayloadAction<string>) => {
      state.locale = action.payload;
    },
    changeNavType: (state, action: PayloadAction<string>) => {
      state.navType = action.payload;
    },
    changeNavTopColor: (state, action: PayloadAction<string>) => {
      state.topNavColor = action.payload;
    },
    changeHeaderNavColor: (state, action: PayloadAction<string>) => {
      state.headerNavColor = action.payload;
    },
    toogleMobileNav: (state, action: PayloadAction<boolean>) => {
      state.mobileNav = action.payload;
    },
    switchTheme: (state, action: PayloadAction<string>) => {
      state.currentTheme = action.payload;
    },
    changeDirection: (state, action: PayloadAction<string>) => {
      state.direction = action.payload;
    },
  },
});

// const theme = (state = initTheme, action) => {
//   switch (action.type) {
//     case TOGGLE_COLLAPSED_NAV:
//       return {
//         ...state,
//         navCollapsed: action.navCollapsed,
//       };
//     case SIDE_NAV_STYLE_CHANGE:
//       return {
//         ...state,
//         sideNavTheme: action.sideNavTheme,
//       };
//     case CHANGE_LOCALE:
//       return {
//         ...state,
//         locale: action.locale,
//       };
//     case NAV_TYPE_CHANGE:
//       return {
//         ...state,
//         navType: action.navType,
//       };
//     case TOP_NAV_COLOR_CHANGE:
//       return {
//         ...state,
//         topNavColor: action.topNavColor,
//       };
//     case HEADER_NAV_COLOR_CHANGE:
//       return {
//         ...state,
//         headerNavColor: action.headerNavColor,
//       };
//     case TOGGLE_MOBILE_NAV:
//       return {
//         ...state,
//         mobileNav: action.mobileNav,
//       };
//     case SWITCH_THEME:
//       return {
//         ...state,
//         currentTheme: action.currentTheme,
//       };
//     case DIRECTION_CHANGE:
//       return {
//         ...state,
//         direction: action.direction,
//       };
//     default:
//       return state;
//   }
// };

export const {
  toogleCollapsedNav,
  toogleMobileNav,
  changeDirection,
  changeHeaderNavColor,
  changeLocal,
  changeNavTopColor,
  changeNavType,
  changeSideNavStyle,
  switchTheme,
} = themeSlice.actions;

export default themeSlice.reducer;
