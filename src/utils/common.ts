import { notification } from "antd"; 

type Placement = 'topRight' | 'topLeft' | 'bottomRight' | 'bottomLeft';

type TypeNoti = 'warn' | 'warning' | 'info' | 'error' | 'success';

export const showNotification = (type: TypeNoti = 'info', message: string = '', placement: Placement = 'topRight') => {
    return notification[type]({message, placement});
}