import React, { useState } from "react";
import { SettingOutlined } from "@ant-design/icons";
import { Avatar, Drawer, Dropdown, Menu } from "antd";
import ThemeConfigurator from "./ThemeConfigurator";
import { useHistory } from "react-router-dom";
import { DIR_RTL } from "../../constants/ThemeConstant";
import {
  UserOutlined,
  SafetyCertificateOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import { useAppSelector } from "../../hooks";

const NavPanel = (props) => {
  const theme = useAppSelector((state) => state.theme);
  const { locale } = theme;
  const [visible, setVisible] = useState(false);
  const history = useHistory();

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const hanldeLoout = async () => {
    try {
      console.log("logout");
      history.push("/auth/login");
    } catch (err) {}
  };

  return (
    <>
      <Menu mode="horizontal">
        <Menu.Item key="panel" onClick={showDrawer}>
          <div>
            <SettingOutlined className="nav-icon mr-0" />
          </div>
        </Menu.Item>
      </Menu>
      <Drawer
        title="Theme Config"
        placement={props.direction === DIR_RTL ? "left" : "right"}
        width={350}
        onClose={onClose}
        visible={visible}
      >
        <ThemeConfigurator />
      </Drawer>
    </>
  );
};

export default NavPanel;
