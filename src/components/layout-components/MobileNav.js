import React from "react";
import { Drawer } from "antd";
import { connect } from "react-redux";
import { NAV_TYPE_SIDE } from "../../constants/ThemeConstant";
import { Scrollbars } from "react-custom-scrollbars";
import MenuContent from "./MenuContent";
import Logo from "./Logo";
import Flex from "../../components/shared-components/Flex";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { useAppDispatch } from "../../hooks";
import { toogleMobileNav } from "../../redux/slices/themeSlice";

export const MobileNav = ({
  sideNavTheme,
  mobileNav,
  routeInfo,
  hideGroupTitle,
  localization = true,
}) => {
  const dispatch = useAppDispatch();
  const props = { sideNavTheme, routeInfo, hideGroupTitle, localization };

  const onClose = () => {
    dispatch(toogleMobileNav(false));
  };

  return (
    <Drawer
      placement="left"
      closable={false}
      onClose={onClose}
      visible={mobileNav}
      bodyStyle={{ padding: 5 }}
    >
      <Flex justifyContent="between" alignItems="center">
        <Logo mobileLogo={true} />
        <div className="nav-close" onClick={() => onClose()}>
          <ArrowLeftOutlined />
        </div>
      </Flex>
      <div className="mobile-nav-menu">
        <Scrollbars autoHide>
          <MenuContent type={NAV_TYPE_SIDE} {...props} />
        </Scrollbars>
      </div>
    </Drawer>
  );
};

const mapStateToProps = ({ theme }) => {
  const { navCollapsed, sideNavTheme, mobileNav } = theme;
  return { navCollapsed, sideNavTheme, mobileNav };
};

export default connect(mapStateToProps)(MobileNav);
