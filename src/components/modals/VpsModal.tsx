import { Form, Input, Modal, Row, Col, Select, Switch, Button } from "antd";
import React, { useCallback, useEffect, useMemo } from "react";
import { FormMode, Vps } from "../../types";

interface Props {
  visible: boolean;
  toogleVisible: (value: boolean) => void;
  mode: FormMode;
  vpsEdit: Vps | null;
}

const VpsModal = (props: Props) => {
  const { visible, toogleVisible, mode, vpsEdit } = props;
  const [form] = Form.useForm();

  useEffect(() => {
    if (!form) return;
    if (mode === "EDIT" && vpsEdit) {
      form.setFieldsValue(vpsEdit);
    } else {
      form.resetFields();
    }
  }, [mode, vpsEdit]);

  const handleCreateVps = useCallback(async (values: any) => {
    try {
    } catch (err) {}
  }, []);

  const handleEditVps = useCallback(async (values: any) => {
    try {
    } catch (err) {}
  }, []);

  return (
    <Modal
      forceRender
      title={mode === "ADD" ? "Thêm VPS" : "Cập nhật VPS"}
      visible={visible}
      footer={false}
      onCancel={() => toogleVisible(false)}
    >
      <Form
        layout="vertical"
        form={form}
        onFinish={mode === "ADD" ? handleCreateVps : handleEditVps}
      >
        <Form.Item
          name="name"
          label="Tên"
          rules={[{ required: true, message: "Vui lòng nhập tên!" }]}
        >
          <Input placeholder="Nhập tên..." />
        </Form.Item>
        <Row gutter={16}>
          <Col xs={24} md={12}>
            <Form.Item
              name="numberOfCore"
              label="Core"
              rules={[{ required: true, message: "VUi lòng chọn số core!" }]}
            >
              <Select placeholder="Chọn số core">
                <Select.Option value="1">1 core</Select.Option>
                <Select.Option value="2">2 core</Select.Option>
                <Select.Option value="3">3 core</Select.Option>
                <Select.Option value="4">4 core</Select.Option>
              </Select>
            </Form.Item>
          </Col>
          <Col xs={24} md={12}>
            <Form.Item
              name="ram"
              label="Ram"
              rules={[{ required: true, message: "VUi lòng chọn ram!" }]}
            >
              <Select placeholder="Chọn ram">
                <Select.Option value="1">1 GB</Select.Option>
                <Select.Option value="2">2 GB</Select.Option>
                <Select.Option value="4">4 GB</Select.Option>
                <Select.Option value="8">8 GB</Select.Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.Item valuePropName="checked" name="status" label="Trạng thái">
          <Switch />
        </Form.Item>
        <div className="d-flex justify-content-end">
          <Button
            size="small"
            onClick={() => {
              toogleVisible(false);
              form.resetFields();
            }}
          >
            Hủy
          </Button>
          <Button className="ml-2" size="small" type="primary">
            Hoàn thành
          </Button>
        </div>
      </Form>
    </Modal>
  );
};

export default VpsModal;
