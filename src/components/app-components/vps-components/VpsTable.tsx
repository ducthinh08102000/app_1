import {
  DeleteOutlined,
  EditOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import { Badge, Button, Input, Select, Table } from "antd";
import React, { Key, useMemo, useState } from "react";
import { Vps } from "../../../types";

interface Props {
  data: Vps[];
  setModeEdit: (data: any) => void;
  toogleVisible: (value: boolean) => void;
}

const fakeData = [
  {
    id: "test1",
    name: "Vps number 1",
    numberOfCore: 2,
    ram: 8,
    hardWare: 250,
    status: true,
  },
  {
    id: "test2",
    name: "Vps number 2",
    numberOfCore: 4,
    ram: 8,
    hardWare: 250,
    status: true,
  },
  {
    id: "test",
    name: "Vps number 3",
    numberOfCore: 2,
    ram: 8,
    hardWare: 250,
    status: true,
  },
  {
    id: "test3",
    name: "Vps number 4",
    numberOfCore: 4,
    ram: 8,
    hardWare: 250,
    status: true,
  },
];

const VpsTable = (props: Props) => {
  const { data, setModeEdit } = props;
  const [selectedRowKeys, setSelectedRowKeys] = useState<Key[]>([]);
  const [filter, setFilter] = useState<string>("all");

  const columns = useMemo(
    () => [
      {
        title: <div className="text-center">Name</div>,
        render: (item: Vps) => {
          return <div className="text-center">{item.name}</div>;
        },
      },
      {
        title: <div className="text-center">CPU</div>,
        render: (item: Vps) => {
          return (
            <div className="d-flex flex-column align-items-center">
              <img
                style={{ height: 22, width: 22 }}
                src={process.env.PUBLIC_URL + "/svg/cpu.svg"}
              />
              <span>{item.numberOfCore} core</span>
            </div>
          );
        },
      },
      {
        title: <div className="text-center">Ram</div>,
        render: (item: Vps) => {
          return (
            <div className="d-flex flex-column align-items-center">
              <img
                style={{ height: 22, width: 22 }}
                src={process.env.PUBLIC_URL + "/svg/ram.svg"}
              />
              <span>{item.ram} GB</span>
            </div>
          );
        },
      },
      {
        title: <div className="text-center">Ổ cứng</div>,
        render: (item: Vps) => {
          return (
            <div className="d-flex flex-column align-items-center">
              <img
                style={{ height: 22, width: 22 }}
                src={process.env.PUBLIC_URL + "/svg/hardware.svg"}
              />
              <span>{item.hardWare} GB</span>
            </div>
          );
        },
      },
      {
        title: <div className="text-center">Trạng thái</div>,
        render: (item: Vps) => {
          return (
            <div className="d-flex justify-content-center">
              {item.status ? (
                <div>
                  <Badge color="green" /> Online
                </div>
              ) : (
                <div></div>
              )}
            </div>
          );
        },
      },
      {
        title: <div className="text-center">Tùy chọn</div>,
        render: (item: Vps) => {
          return (
            <div className="d-flex justify-content-center">
              <Button
                onClick={() => {
                  setModeEdit(item);
                }}
                className="mx-2"
                icon={<EditOutlined />}
              />
              <Button danger icon={<DeleteOutlined />} />
            </div>
          );
        },
      },
    ],
    []
  );

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center">
        <div className="d-flex align-items-center">
          {selectedRowKeys.length > 0 && (
            <div className="d-flex align-items-center mr-2">
              <DeleteOutlined
                className="cursor-pointer"
                style={{ fontSize: 20 }}
              />
            </div>
          )}
          <Select
            size="small"
            value={filter}
            onChange={(value) => setFilter(value)}
          >
            <Select.Option value="all">Tất cả</Select.Option>
          </Select>
          <Input
            className="ml-2"
            size="small"
            prefix={<SearchOutlined />}
            placeholder="Tìm kiếm..."
          />
        </div>
        <div></div>
      </div>
      <Table
        rowSelection={{
          selectedRowKeys: selectedRowKeys,
          onChange: (data) => {
            setSelectedRowKeys(data);
          },
        }}
        rowKey={"id"}
        columns={columns}
        dataSource={fakeData}
      />
    </div>
  );
};

export default React.memo(VpsTable);
