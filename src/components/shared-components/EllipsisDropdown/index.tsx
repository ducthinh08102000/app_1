import React from "react";
import { Dropdown, Menu } from "antd";
import { EllipsisOutlined } from "@ant-design/icons";
import PropTypes from "prop-types";

interface Props {
  icon?: any;
  placement?:
    | "bottomRight"
    | "topLeft"
    | "topCenter"
    | "topRight"
    | "bottomLeft"
    | "bottomCenter";
  menu: any;
}

const EllipsisDropdown = (props: Props) => {
  return (
    <Dropdown
      overlay={props.menu}
      placement={props.placement}
      trigger={["click"]}
    >
      <div className="ellipsis-dropdown">
        {props.icon ? props.icon : <EllipsisOutlined />}
      </div>
    </Dropdown>
  );
};

EllipsisDropdown.propTypes = {
  trigger: PropTypes.string,
  placement: PropTypes.string,
};

EllipsisDropdown.defaultProps = {
  trigger: "click",
  placement: "bottomRight",
  menu: <Menu />,
};

export default EllipsisDropdown;
