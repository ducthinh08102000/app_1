import React from 'react'

interface ThinhLoz {
	className: string,
	alignItems: string,
	flexDirection: string,
	justifyContent: string,
	mobileFlex: boolean,
	children: any
}

const Flex = (props: ThinhLoz) => {
	let { children, className, alignItems, justifyContent, mobileFlex, flexDirection } = props;
	if (typeof mobileFlex === "undefined") mobileFlex = true;
	if (typeof flexDirection === "undefined") flexDirection = 'row';
	if (typeof className === "undefined") className = "";
	const getFlexResponsive = () => mobileFlex ? 'd-flex' : 'd-md-flex';
	let classes = getFlexResponsive() + " " + className + (flexDirection?('flex-' + flexDirection): '') + " "  + (alignItems?('align-items-' + alignItems):'') + " "  + (justifyContent?('justify-content-' + justifyContent):'');
	return (
		<div className={classes}>
			{children}
		</div>
	)
};

export default Flex;
