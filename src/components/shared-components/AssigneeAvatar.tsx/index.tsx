import { Avatar, Tooltip } from "antd";
import React from "react";

interface Props {
  size?: number;
  chain?: boolean;
  maxAvatar?: number;
  userList: {
    name: string;
    img: string;
    id: string;
  }[];
}

const AssigneeAvatar = (props: Props) => {
  const { maxAvatar } = props;
  const size = props?.size ?? 20;
  const chain = props.chain ? true : false;
  const userList = maxAvatar
    ? props.userList.slice(0, maxAvatar)
    : props.userList;

  if (userList.length > 0) {
    return (
      <div className="d-flex">
        {userList.map((x) => {
          const { id, name, img } = x;
          return (
            <div key={id} className={`d-flex ${chain ? "ml-n2" : ""}`}>
              <Tooltip title={name}>
                <Avatar
                  className="cursor-pointer"
                  size={size}
                  src={img}
                  style={chain ? { border: "2px solid #fff" } : {}}
                ></Avatar>
              </Tooltip>
            </div>
          );
        })}
        {props.maxAvatar && props.userList.length > props.maxAvatar && (
          <div className={`d-flex ${chain ? "ml-n2" : ""}`}>
            <Avatar
              className="cursor-pointer"
              size={size}
              style={chain ? { border: "2px solid #fff" } : {}}
            >
              +{props.userList.length - props.maxAvatar}
            </Avatar>
          </div>
        )}
      </div>
    );
  } else return null;
};

export default AssigneeAvatar;
