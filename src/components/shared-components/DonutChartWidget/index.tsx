import React from "react";
import { Card } from "antd";
import ApexChart from "react-apexcharts";
import { apexPieChartDefaultOption } from "../../../constants/ChartConstant";
import { ApexOptions } from "apexcharts";
import type {Props as ApexProps} from "react-apexcharts";

const defaultOption = apexPieChartDefaultOption;

const Chart = (props: ApexProps) => {
  return <ApexChart {...props} />;
};

interface Props {
  series: ApexOptions['series'],
  customOptions: ApexOptions,
  labels: any,
  width: string,
  height: number,
  title: string,
  extra: any,
  bodyClass: string,
}

const DonutChartWidget = (props: Props) => {
  let {
    customOptions,
    extra,
    bodyClass,
  } = props;
  let series = props.series || [],
  labels = props.labels || [],
  title = props.title || "",
  height = props.height || 250,
  width = props.width || "100%";

  let options: ApexProps = defaultOption;
  options.labels = labels;
  options.plotOptions.pie.donut.labels.total.label = title;
  if (!title) {
    options.plotOptions.pie.donut.labels.show = false;
  }
  if (customOptions) {
    options = { ...options, ...customOptions };
  }
  return (
    <Card>
      <div className={`text-center ${bodyClass}`}>
        <div className="d-flex justify-content-center">
          <Chart
            type="donut"
            options={options}
            series={series}
            width={width}
            height={height}
          />
        </div>
        {extra}
      </div>
    </Card>
  );
};

export default DonutChartWidget;
