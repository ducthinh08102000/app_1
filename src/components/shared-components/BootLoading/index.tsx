import React from "react";
import { motion } from "framer-motion";

const BootLoading = () => {
  const transitionValues = {
    duration: 0.8,
    yoyo: Infinity,
    ease: "easeOut",
  };

  const ballStyle = {
    display: "block",
    backgroundColor: "transparent",
    borderRadius: "5rem",
    marginRight: 'auto',
    marginLeft: 'auto',
    height: 130,
    width: 130,
  };

  return (
    <div style={{ height: "100vh", width: "100vw", display: "flex" }}>
      <div style={{margin: 'auto'}}>
        <motion.span
          style={ballStyle}
          transition={{
            y: transitionValues,
            width: transitionValues,
            height: transitionValues,
          }}
          animate={{
            y: ["2rem", "-1rem"],
          }}
        >
          <img
            src={process.env.PUBLIC_URL + '/img/north-studio-logo.png'}
            height={130}
            width={130}
            alt="logo"
          />
        </motion.span>
        <p className="fw-bold" style={{fontSize: 20}}>NorthStudio TTAS., JSC</p>
      </div>
    </div>
  );
};

export default BootLoading;
