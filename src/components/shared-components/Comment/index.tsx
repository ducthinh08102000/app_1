import { Avatar, Button, Input } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Comment } from "antd";
import { SendOutlined } from "@ant-design/icons";

interface Props {
  children?: React.Component;
}

const AppComment = (props: Props) => {
  const { children } = props;
  const [showInput, setShowInput] = useState<boolean>(false);
  const [comment, setComment] = useState<string>("");

  const handleComment = async () => {
    try {
      if (comment === "") return;
      console.log(comment);
    } catch (err) {
      console.log(err);
    }
    setShowInput(false);
    setComment('');
  };

  return (
    <Comment
      actions={[
        <span
          onClick={() => setShowInput((state) => !state)}
          key="comment-nested-reply-to"
          className="reply-button"
        >
          Reply
        </span>,
      ]}
      author={<Link to="/account/test">Han Solo</Link>}
      avatar={
        <Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />
      }
      content={
        <p>
          We supply a series of design principles, practical patterns and high
          quality design resources (Sketch and Axure).
        </p>
      }
    >
      {showInput && (
        <div className="d-flex align-items-start">
          <Input.TextArea
            value={comment}
            onChange={(e) => setComment(e.target.value)}
            placeholder="Nhập bình luận..."
            className="flex-grow-1 mr-2"
          />
          <Button
            onClick={handleComment}
            size="small"
            icon={<SendOutlined />}
            shape="circle"
            type="primary"
          ></Button>
        </div>
      )}
      {children ?? null}
    </Comment>
  );
};

export default AppComment;
