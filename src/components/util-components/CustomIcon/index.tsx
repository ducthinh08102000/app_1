import React, { ComponentType, CSSProperties, ForwardRefExoticComponent, SVGProps } from 'react'
import Icon from '@ant-design/icons';
import { Component } from 'react';
import { CustomIconComponentProps } from '@ant-design/icons/lib/components/Icon';

const CustomIcon = React.forwardRef((props: {style?: CSSProperties, className: string, svg: ComponentType<CustomIconComponentProps | SVGProps<SVGSVGElement>> | ForwardRefExoticComponent<CustomIconComponentProps> | undefined}, _) => <Icon style={props.style} component={props.svg} className={props.className}/>)

export default CustomIcon
