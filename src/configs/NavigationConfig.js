import {
  HomeOutlined,
} from "@ant-design/icons";

const dashBoardNavTree = [
  {
    key: "home",
    path: `/home`,
    title: "Home",
    icon: HomeOutlined,
    breadcrumb: false,
    submenu: [],
  },
  {
    key: "vps",
    path: `/vps`,
    title: "VPS",
    icon: HomeOutlined,
    breadcrumb: false,
    submenu: [],
  },
];

const navigationConfig = [...dashBoardNavTree];

export default navigationConfig;
